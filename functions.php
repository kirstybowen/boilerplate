<?php
/**
 * bluestag functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package bluestag
 */
  

// Theme support options
require_once(get_template_directory().'/functions/theme-support.php'); 

// WP Head and other cleanup functions
require_once(get_template_directory().'/functions/cleanup.php');  

// Register scripts and stylesheets
require_once(get_template_directory().'/functions/enqueue-scripts.php'); 

// Register custom menus and menu walkers
require_once(get_template_directory().'/functions/bs-boilerplate-menu.php'); 
 
 

// Replace 'older/newer' post links with numbered navigation
require_once(get_template_directory().'/functions/bs-boilerplate-page-navi.php'); 
 
//add additional functions by Blue Stag
require_once(get_template_directory().'/functions/bs-boilerplate-bespoke.php'); 
 
//add additional functions that power archives
require_once(get_template_directory().'/functions/bs-boilerplate-archives.php'); 
  
// helpers for the theme - e.g. outputting buttons
require_once(get_template_directory().'/functions/bs-boilerplate-helpers.php'); 

// code unique to this project
require_once(get_template_directory().'/functions/project-unique.php'); 








if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

 
 
 