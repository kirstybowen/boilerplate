<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package bluestag
 */

get_header();


// outputs the post page banner
kb_generate_banner(get_post_type());

// output the page if it's not password protected
if(kb_not_password()){
	
	if (have_posts()) {
		while ( have_posts() ) :
			the_post();

			get_template_part( 'parts/single/single', get_post_type() );

		endwhile; 
	}
}

get_footer();

?>
