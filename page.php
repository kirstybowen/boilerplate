<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bluestag
 */


get_header();

// outputs the default page banner
kb_generate_banner();

// output the page if it's not password protected
if(kb_not_password()){
	
	if (have_posts()) {
		while ( have_posts() ) :
			the_post();

			//OUTPUT THE PAGE BLOCKS
			bs_flex_master(); 


		endwhile;  
	}

}

get_footer();

?>