<?php 
/**
 * The template for displaying the 404 page
 */

	// HEADER
	get_header();

	// ERROR 404 UI
	get_template_part('parts/common/ui-e404');

	// FOOTER
	get_footer();

?>