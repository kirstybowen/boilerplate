<?php

get_header();

// outputs the default page banner
kb_generate_banner();


// output the page if it's not password protected
if(kb_not_password()){

// outputs the filters
	kb_generate_filters(); 

	?>
	<div id='listing-section-ajax-holder'>
		<?php
		kb_generate_listings('post' ,get_option( 'page_for_posts' ));
		?>
	</div>
	<?php
}

get_footer();

?>