
	/////////////////////////////////////
	// DETECTING PAGE SCROLL DIRECTION //
	/////////////////////////////////////
	var last_scroll = 0;
	var last_scroll_direction = "up";

	function detectScroll() {

		if ( !$("body").hasClass("nav-open") ) {

			// DEFINE THE SCROLL DIRECTION
			if ( current_scroll >= 64 ) {

				$("body").addClass("scrolled");

			} else {

				$("body").removeClass("scrolled");

			}

			// THE POINT AT WHICH TO SCALE THE HEADER
			var scroll_limit = 64;
			if ( $("body").hasClass("home") && $(".home-hero").length > 0 ) { scroll_limit = $(".home-hero").outerHeight(); }

			// SCALE HEADER IF SCROLLED DOWN > 100px
			if ( current_scroll > scroll_limit )
				{ $("body").addClass("scale-header"); } else
				{ $("body").removeClass("scale-header"); }

			// DEFINE THE SCROLL DIRECTION
			if ( current_scroll >= 200 ) {

				// SCROLL DOWN
				if ( current_scroll > last_scroll) {
					last_scroll = current_scroll;
					last_scroll_direction = "down";
					if ( !$("body").hasClass("scroll-down") ) {
						$("body").addClass("scroll-down");
					}
				}

				// SCROLL UP
				else {

					var difference = Math.abs(current_scroll - last_scroll);

					if ( difference > 200 ) {
						last_scroll = current_scroll;
						last_scroll_direction = "up";
						if ( $("body").hasClass("scroll-down") ) {
							$("body").removeClass("scroll-down");
						}

					}

				}

			} else {

				if ( $("body").hasClass("scroll-down") ) {
					$("body").removeClass("scroll-down");
				}

			}

			

		} else {

			if ( $("body").hasClass("scroll-down") ) {
				$("body").removeClass("scroll-down");
			}

			if ( $("body").hasClass("scale-header") ) {
				$("body").removeClass("scale-header");
			}

		}

	}

	
	///////////////////
	// HEADER RESIZE //
	///////////////////
	function resizeHeader() {

		// // RESET
		// $("body").removeClass("disable-nav");

		// // GET
		// var header_width = parseInt($("header-inner").width());
		// var lang_width = parseInt($("header ul.lang").outerWidth());
		// var link_padding = parseInt($("header nav li a").css("padding-right"));
		// var logo_width = parseInt($("header a.logo").outerWidth());
		// var logo_padding = parseInt($("header a.logo").css("padding-right"));

		// // GET NAV WIDTH
		// var nav_width = link_padding;
		// $("header nav > ul > li").each(function(){
		// 	nav_width += parseInt($(this).outerWidth());
		// });

		// // THE OVERALL WIDTH OF THE CONTENT
		// var combined_width = nav_width + lang_width + logo_width + logo_padding;

		// // IF TOO LONG, SWITCH TO THE MOBILE VERSION
		// if ( header_width < combined_width ) { $("body").addClass("disable-nav"); }

		// // 
		// if ( !$("body").hasClass("disable-nav") && $("body").hasClass("nav-open") ) { $("body").removeClass("nav-open"); $.scrollLock(false); }

	}


	/////////////////////
	// INIT SCROLLBARS //
	/////////////////////
	function initScrollbars() {

		// $(".flex.table").each(function(){
		// 	$(this).find(".scroll").scrollbar();
		// });

	}


	//////////////////////
	// MOBILE NAV SETUP //
	//////////////////////
	function setupMobileNav() {

		// $("header nav").clone().appendTo("mobile-nav nav-inner");
		// $("footer nav.social").clone().appendTo("mobile-nav nav-inner");
		// $("header ul.lang").clone().appendTo("mobile-nav nav-inner");

		// $("mobile-nav li.menu-item-has-children").each(function(){
		// 	$("<button type=\"button\" data-action=\"toggle-children\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"#000000\" stroke-width=\"3\" stroke-linecap=\"square\" stroke-linejoin=\"arcs\"><path d=\"M6 9l6 6 6-6\"/></svg></button>").insertAfter($(this).find(" > a"));
		// });

		// if ( $("mobile-nav .lang-item a").text() == "cy" )
		// 	{ $("mobile-nav .lang-item a").text("Cymraeg"); $("mobile-nav #main-nav ul").prepend("<li><a href=\"" + $("mobile-nav").attr("data-url") + "\">Home</a></li>"); } else
		// 	{ $("mobile-nav .lang-item a").text("English"); $("mobile-nav #main-nav ul").prepend("<li><a href=\"" + $("mobile-nav").attr("data-url") + "\">Cartref</a></li>"); }

	}


	///////////////////////
	// TOGGLE MOBILE NAV //
	///////////////////////
	$("button[data-action=\"toggle-nav\"]").click(function(e){ e.preventDefault();

		$("body").toggleClass("nav-open");

	});

	/////////////////////////////
	// TOGGLE MOBILE NAV CHILD //
	/////////////////////////////
	$("body").on("click", "button[data-action=\"toggle-children\"]", function(e){ e.preventDefault();

		$(this).toggleClass("show-children");

	});

	/////////////////////////////
	// TOGGLE MOBILE NAV CHILD //
	/////////////////////////////
	$("body").on("click", "mobile-nav a[href=\"#\"]", function(e){ e.preventDefault();

		$(this).next("button").toggleClass("show-children");

	});


	///////////////////////
	// LOTTIE ANIMATIONS //
	///////////////////////
	function lottieAnimations() {

		// // INITIALISE ALL THE LOTTIE ANIMS
		// $("div[data-anim]").each(function(){

		// 	var this_id = "anim_" + animID;
		// 	var animJSON = JSON.parse(atob($(this).attr("data-anim")));

		// 	// REMOVE ANIMATION JSON FROM DOM
		// 	$(this).removeAttr("data-anim");

		// 	// SET THE ID
		// 	$(this).attr("id", this_id);

		// 	// SETUP THE ANIMTAION
		// 	animations[this_id] = bodymovin.loadAnimation({
		// 		container: document.getElementById(this_id),
		// 		renderer: 'svg',
		// 		loop: false,
		// 		autoplay: false,
		// 		animationData: animJSON,
		// 	});

		// 	// INCREMENT
		// 	animID++;

		// 	// ONLY PLAY THE FIRST HALF OF THE LOGO
		// 	animations[this_id].goToAndStop(animations[this_id].totalFrames, true);

		// });

	}


	function linkSecurity() {

		$("a[href]").each(function(){

			if ( $(this).attr("target") == "_blank" ) {

				$(this).attr("rel","noopener");

			}

		});

	}