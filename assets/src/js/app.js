
	// JS LIBS
	// @koala-prepend "jquery.scrolllock.min.js"
	// @koala-prepend "jquery.slick.min.js"
	// @koala-prepend "jquery.scrollbar.min.js"

	// CORE THEME JS
	// @koala-prepend "app.common.js"

	// FLEX BLOCKS
	// @koala-prepend "app.flex.slideshows.js"

	// LEGACY SUPPORT
	// @koala-prepend "app.ie.js"


	/////////////////////
	///// PAGE VARS /////
	/////////////////////

	// VIEWPORT WIDTH
	var old_viewport_width = false;
	var viewport_width = $("body").outerWidth();

	// VIEWPORT HEIGHT
	var viewport_height = window.innerHeight;
	var viewport_midpoint = viewport_height / 2;

	// CURRENT SCROLL
	var current_scroll = parseInt($(document).scrollTop());

	// DOCUMENT HEIGHT
	var page_height = $("body").outerHeight();

	// HEADER HEIGHT
	var header_height = $("header").outerHeight();

	// VIDEO PLAYERS
	// var videoPlayers = [];
	// var videoID = 1;

	// ANIMATIONS
	// var animations = {};
	// var animID = 1;

	// SVGS
	var svgs = {};		
		svgs.linkedin = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200 200"><path d="M185,118.4v62.9h-36.4v-58.6c0-14.7-5.3-24.8-18.5-24.8c-10.1,0-16.1,6.8-18.7,13.3c-1,2.3-1.2,5.6-1.2,8.9 v61.2H73.8c0,0,0.5-99.3,0-109.6h36.4v15.5c-0.1,0.1-0.2,0.2-0.2,0.4h0.2v-0.4C115,79.7,123.7,69,143,69C167,69,185,84.7,185,118.4 z M35.6,18.8C23.2,18.8,15,27,15,37.7c0,10.5,7.9,18.9,20.1,18.9h0.2c12.7,0,20.6-8.4,20.6-18.9C55.8,27,48.1,18.8,35.6,18.8z M17.2,181.2h36.4V71.6H17.2V181.2z"></path></svg>';
		svgs.instagram = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M352,0H160C71.648,0,0,71.648,0,160v192c0,88.352,71.648,160,160,160h192c88.352,0,160-71.648,160-160V160 C512,71.648,440.352,0,352,0z M464,352c0,61.76-50.24,112-112,112H160c-61.76,0-112-50.24-112-112V160C48,98.24,98.24,48,160,48 h192c61.76,0,112,50.24,112,112V352z"></path><path d="M256,128c-70.688,0-128,57.312-128,128s57.312,128,128,128s128-57.312,128-128S326.688,128,256,128z M256,336 c-44.096,0-80-35.904-80-80c0-44.128,35.904-80,80-80s80,35.872,80,80C336,300.096,300.096,336,256,336z"></path><circle cx="393.6" cy="118.4" r="17.056"></circle></svg>';
		svgs.twitter = '<svg viewBox="0 0 17 17" xmlns="http://www.w3.org/2000/svg"><path d="M 16.8313 4.1875 C 16.2196 4.4609 15.5624 4.645 14.8726 4.728 C 15.5767 4.3037 16.1173 3.6309 16.3719 2.8291 C 15.7131 3.2227 14.9833 3.5078 14.2064 3.6621 C 13.5844 2.9951 12.698 2.5776 11.717 2.5776 C 9.8336 2.5776 8.3069 4.1143 8.3069 6.0107 C 8.3069 6.2803 8.3369 6.542 8.395 6.7935 C 5.5608 6.6504 3.0479 5.2832 1.3657 3.2061 C 1.0723 3.7134 0.9041 4.3027 0.9041 4.9326 C 0.9041 6.123 1.5062 7.1738 2.4213 7.79 C 1.8622 7.7725 1.3364 7.6182 0.8767 7.3604 C 0.8762 7.375 0.8762 7.3896 0.8762 7.4038 C 0.8762 9.0674 2.0519 10.4551 3.6119 10.77 C 3.3257 10.8491 3.0245 10.8906 2.7135 10.8906 C 2.4937 10.8906 2.2802 10.8691 2.0719 10.8291 C 2.5059 12.1934 3.7653 13.1855 5.2576 13.2139 C 4.0905 14.1348 2.62 14.6831 1.0221 14.6831 C 0.7469 14.6831 0.4756 14.667 0.2086 14.6353 C 1.718 15.6094 3.5105 16.1777 5.4364 16.1777 C 11.7092 16.1777 15.1394 10.9463 15.1394 6.4092 C 15.1394 6.2607 15.1361 6.1123 15.1295 5.9648 C 15.7958 5.4805 16.374 4.876 16.8313 4.1875 Z"></path></svg>';
		svgs.fb = '<svg viewBox="0 0 17 17" xmlns="http://www.w3.org/2000/svg"><path d="M 10.7169 3.4766 L 12.8542 3.4766 L 12.8542 0.311 L 10.3422 0.311 L 10.3422 0.3232 C 7.2998 0.4282 6.6744 2.1455 6.6199 3.9468 L 6.6135 3.9468 L 6.6135 5.5254 L 4.5431 5.5254 L 4.5431 8.626 L 6.6135 8.626 L 6.6135 16.9336 L 9.7393 16.9336 L 9.7393 8.626 L 12.2979 8.626 L 12.7897 5.5254 L 9.7393 5.5254 L 9.7393 4.5723 C 9.7393 3.9634 10.1421 3.4766 10.7169 3.4766 Z"></path></svg>';
		svgs.youtube = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M23.495 6.205a3.007 3.007 0 0 0-2.088-2.088c-1.87-.501-9.396-.501-9.396-.501s-7.507-.01-9.396.501A3.007 3.007 0 0 0 .527 6.205a31.247 31.247 0 0 0-.522 5.805 31.247 31.247 0 0 0 .522 5.783 3.007 3.007 0 0 0 2.088 2.088c1.868.502 9.396.502 9.396.502s7.506 0 9.396-.502a3.007 3.007 0 0 0 2.088-2.088 31.247 31.247 0 0 0 .5-5.783 31.247 31.247 0 0 0-.5-5.805zM9.609 15.601V8.408l6.264 3.602z"></path></svg>';
		svgs.tiktok = '<svg viewBox="0 0 17 17" xmlns="http://www.w3.org/2000/svg"><path d="M14.4,2.9c-0.9,0-1.7-0.7-1.9-1.6c0-0.4-0.1-0.7,0-1.1c0-0.1,0-0.1,0-0.2H9.1c0,0,0,7.6,0,11.4 c0,0.9-0.6,1.8-1.5,2.1c-1.1,0.3-2.2-0.3-2.5-1.4c-0.1-0.3-0.1-0.6,0-0.9c0.2-1.1,1.2-1.8,2.3-1.6c0,0,0,0,0,0 c0.4,0.1,0.7,0.2,1,0.5V6.5C7.6,6.2,6.7,6.1,5.9,6.3C3,7,1.2,9.9,1.9,12.8c0.1,0.4,0.2,0.7,0.4,1.1c1.1,2.6,4,3.8,6.6,2.8 c0.3-0.1,0.5-0.2,0.8-0.4c1.8-0.9,2.9-2.7,2.9-4.7c0-1.8,0-3.6,0-5.4V5.9l0,0c0.7,0.3,1.5,0.4,2.3,0.3l0.6,0V2.9 C15,2.9,14.7,2.9,14.4,2.9z"></path></svg>';
		svgs.website = '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#00D2AA" stroke-width="1.5" xmlns="http://www.w3.org/2000/svg"><circle cx="12" cy="12" r="10.9686"/><path d="M12 1.05661L12 22.9434"/><path d="M11.4465 1.05661C11.4465 1.05661 6.18868 4.55114 6.18868 12C6.18868 19.4489 11.4465 22.9434 11.4465 22.9434"/><path d="M12.5535 1.05661C12.5535 1.05661 17.7862 4.55114 17.7862 12C17.7862 19.4489 12.5535 22.9434 12.5535 22.9434"/><path d="M1.03143 12L22.9434 12"/><path d="M3.522 5.10693C3.522 5.10693 6.66122 8.05033 11.9623 8.05033C17.2633 8.05033 20.4025 5.10693 20.4025 5.10693"/><path d="M20.4025 18.8931C20.4025 18.8931 17.2633 15.9497 11.9623 15.9497C6.66124 15.9497 3.52201 18.8931 3.52201 18.8931"/></svg>';


	/////////////////////
	///// DETECT IE /////
	/////////////////////
	function detectIE() {
		var a = window.navigator.userAgent,
		b = a.indexOf("MSIE ");
		if (b > 0) return parseInt(a.substring(b + 5, a.indexOf(".", b)), 10);
		var c = a.indexOf("Trident/");
		if (c > 0) {
			var d = a.indexOf("rv:");
			return parseInt(a.substring(d + 3, a.indexOf(".", d)), 10)
		}
		var e = a.indexOf("Edge/");
		return e > 0 && parseInt(a.substring(e + 5, a.indexOf(".", e)), 10)
	}

	// RUN DETECT IE(X)
	var ieVersion = parseInt(detectIE());
	if ( !isNaN(ieVersion) && ( ieVersion !== 1 || ieVersion < 12 ) ) { $("html").addClass("is-ie ie" + ieVersion); }


	/////////////////////////
	///// DETECT SAFARI /////
	/////////////////////////
	function detectSafariChrome() {
		var ua = navigator.userAgent.toLowerCase(); 
		if (ua.indexOf('safari') != -1) { 
			if (ua.indexOf('chrome') > -1) {
				return "chrome";
			} else {
				return "safari";
			}
		}
	}

	// RUN DETECT SAFARI / CHROME
	var csVersion = detectSafariChrome();
	$("html").addClass(csVersion);


	// PREVENT CLICK ON NON-LINKED TOP LEVEL NAV
	$("nav a[href=\"#\"]").click(function(e){
		e.preventDefault();
	});


	////////////////////
	// FACEBOOK SHARE //
	////////////////////
	// window.fbAsyncInit = function() {
	// 	FB.init({
	// 		appId            : '1831646523711965',
	// 		autoLogAppEvents : true,
	// 		xfbml            : true,
	// 		version          : 'v3.2'
	// 	});
	// };

	// (function(d, s, id){
	// 	var js, fjs = d.getElementsByTagName(s)[0];
	// 	if (d.getElementById(id)) {return;}
	// 	js = d.createElement(s); js.id = id;
	// 	js.src = "https://connect.facebook.net/en_US/sdk.js";
	// 	fjs.parentNode.insertBefore(js, fjs);
	// }(document, 'script', 'facebook-jssdk'));


	// $("ul.share a.facebook").click(function(event){
	// 	event.preventDefault();
	// 	var url = $(this).attr("href");
	// 	FB.ui({
	// 		method: 'share',
	// 		href: url,
	// 	}, function(response){});
	// });


	/////////////////////////////////////////////////
	// ADD FUNCTIONS TO RUN ON DOCUMENT READY HERE //
	/////////////////////////////////////////////////
	function runOnReveal() {

		////////////////////////
		// VIDEO PLAYER SETUP //
		////////////////////////

		// ENABLE IF USING PLYR
		// $('.plyr__video-embed').each(function(){

		// 	var id = "player_" + videoID;
		// 	$(this).attr("id", id);
		// 	videoID++;

		// 	videoPlayers[id] = new Plyr($(this), {
		// 		quality: 720,
		// 		clickToPlay: true
		// 	});

		// });


		///////////////////
		// 25MS DEFERRED //
		///////////////////
		setTimeout(function(){

			// COMMON
			linkSecurity();
			fallbackImagery();
			detectScroll();
			resizeHeader();
			initScrollbars();
			lottieAnimations();
			enableSlideshows();

			// LOADED CLASS
			$("body").addClass("loaded");

		},25);

	}


	/////////////////////////////////////////
	// ADD FUNCTIONS TO RUN ON SCROLL HERE //
	/////////////////////////////////////////
	function runOnScroll() {

		// DISABLE TICK FOR NEXT SCROLL
		ticking = false;

		if ( $("body").hasClass("loaded") ) {

			// SCROLL DIRECTION DETECTION
			detectScroll();

			// SECTIONS
			timelineScroll();
			homeHeroScroll();

		}

	}

	/////////////////////////////////////////
	// ADD FUNCTIONS TO RUN ON RESIZE HERE //
	/////////////////////////////////////////
	function runOnResize() {

		// COMMON
		resizeHeader();
		resizeSubmenu();
		timelineScroll();
		homeHeroScroll();
		homeHeroResize();

	}


	/////////////////////////////////////////////////////////////
	///////////////////// ANIMATION HANDLER /////////////////////
	/////////////////////////////////////////////////////////////
	var ticking = false;


	////////////////////////////////////
	// INITIAL FUNCITON RUN ON SCROLL //
	////////////////////////////////////
	function onScroll() {
		
		// UPDATE SCROLL
		current_scroll = parseInt($(document).scrollTop());

		// REQUEST ANIMATION TICK
		requestTick();

	}


	/////////////////////////////
	// ANIMATION TICK FUNCTION //
	/////////////////////////////
	function requestTick() {

		// RAF IF NOT ALREADY TICKING
		if ( !ticking ) { requestAnimationFrame(runOnScroll); }

		// ENABLE TICK
		ticking = true;
	}


	///////////////////////
	// ON DOCUMENT READY //
	///////////////////////
	$(document).ready(function(){

		// RUN ON REVEAL FUNCTION
		runOnReveal();

	});


	///////////////////////////////
	// GLOBAL 'ON SCROLL' EVENTS //
	///////////////////////////////
	$(window).scroll(function(){

		// UPDATE CURRENT SCROLL VAR
		current_scroll = parseInt($(document).scrollTop());

		// SCROLL ANIMATION HANDLER
		onScroll();

	});


	///////////////////////////////
	// GLOBAL 'ON RESIZE' EVENTS //
	///////////////////////////////
	var resizeTimer;
	var doneResizing = 50;
	$(window).resize(function(){

		// ADD CLASS
		if ( !$("body").hasClass("is-resizing") ) { $("body").addClass("is-resizing"); }

		// RESET THE TIMER
		clearTimeout(resizeTimer);

		// RESTART THE TIMER
		resizeTimer = setTimeout(function(){

			// SCREEN VARS
			old_viewport_width = viewport_width;
			viewport_width = $("body").outerWidth();
			viewport_height = window.innerHeight;
			viewport_midpoint = viewport_height / 2;
			page_height = $("body").outerHeight();
			header_height = $("header").outerHeight();

			// RUN ON RESIZE
			runOnResize();

			// REMOVE CLASS
			$("body").removeClass("is-resizing");

		}, doneResizing);

	});
