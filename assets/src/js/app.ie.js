	
	/////////////////////////////////////////////////////
	// FALLBACK IMAGERY FOR IE AND OLDER EDGE BROWSERS //
	/////////////////////////////////////////////////////
	function fallbackImagery() {

		// ONLY RUN IF NOT IE
		if ( !isNaN(ieVersion) && ieVersion < 16 ) {

			$("figure:not(.fallback)").each(function(){

				// IF HAS A CHILD IMG
				if ( $(this).find("> img").length == 1 ) {

					// GET THE IMAGE URL
					var url = $(this).find("> img").attr("data-src");

					// IF WE HAVE A VALID URL
					if ( url !== undefined ) {

						// REMOVE THE IMAGE
						$(this).empty();

						// SET THE FIGURE IMAGE URL
						$(this).css("background-image", "url('" + url + "')");

						// ADD THE IMAGE
						$(this).addClass("fallback");

					} else {

						var url = $(this).find("> img").attr("src");

						// IF WE HAVE A VALID URL
						if ( url !== undefined ) {

							// REMOVE THE IMAGE
							$(this).empty();

							// SET THE FIGURE IMAGE URL
							$(this).css("background-image", "url('" + url + "')");

							// ADD THE IMAGE
							$(this).addClass("fallback");

						} 

					}

				}

			});

		}

	}
