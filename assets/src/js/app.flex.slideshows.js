
	////////////////
	// SLIDESHOWS //
	////////////////
	function enableSlideshows() {

		/////////////
		// GALLERY //
		/////////////
		if ( $('.flex.gallery').length > 0 ) {

			// FOR EACH
			$('.flex.gallery .slideshow-container').each(function(){

				if ( $(this).find(".slide").length > 1 ) {

					$(this).slick({
						slide: '.slide',
						dots: true,
						arrows: true,
						prevArrow: "<button class=\"arrow prev\"><span>Previous</span><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"#000000\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\"><path d=\"M19 12H6M12 5l-7 7 7 7\"/></svg></button>",
						nextArrow: "<button class=\"arrow next\"><span>Next</span><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"#000000\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\"><path d=\"M5 12h13M12 5l7 7-7 7\"/></svg></button>",
						fade: false,
						infinite: true,
						speed: 250,
						autoplay: false,
						slidesToShow: 1,
						slidesToScroll: 1,
						variableWidth: true,
						centerMode: true,
					});

				}

			});

			// UPDATE THE PAGE HEIGHT
			page_height = $("body").outerHeight();

		}

	}
