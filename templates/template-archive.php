<?php
/* Template Name: Archive Template */
 
get_header();
 
kb_generate_banner();


// output the page if it's not password protected
if(kb_not_password()){

	
	if (have_posts()) {
 
		while ( have_posts() ) {
			
			the_post();

			// THE POST TYPE OF THE ARCHIVE
			$archive_type = get_field('archive_type');

			// outputs the filters
			kb_generate_filters($archive_type); 
 
			// OUTPUT THE LISTINGS
			?>
			<div id='listing-section-ajax-holder'>
				<?php
				kb_generate_listings($archive_type, get_the_id());
				?>
			</div>

			<?php
		// FLEX CONTENT
			bs_flex_master(); 

		}
	}

} 
get_footer(); 

?>