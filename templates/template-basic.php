<?php
/* Template Name: Basic Template */
/***/

get_header();

kb_generate_banner();

// output the page if it's not password protected
if(kb_not_password()){

	if ( have_posts() ) {
		while ( have_posts() ) :
			the_post(); 


			ob_start();
			the_content();
			$content_output = ob_get_clean(); 
			get_template_part('parts/flex/block','text_editor',['content'=>$content_output]);

		endwhile;  
	}
} 

get_footer();

?>