<?php
/* Template Name: HomePage Template */
/**
 
 *
 * @package bluestag
 */

get_header();

// outputs the default page banner
kb_generate_banner();

// output the page if it's not password protected
if(kb_not_password()){
 
	if ( have_posts() ) {
		while ( have_posts() ) :
			the_post();

			//OUTPUT THE PAGE BLOCKS
			bs_flex_master(); 			

		endwhile;  
	}
} 
 
get_footer();

?>