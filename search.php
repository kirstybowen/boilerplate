<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package bluestag
 */

get_header();


// outputs the search banner
kb_generate_banner('search');


// output the search filters
kb_generate_filters('search');  

?> 
<div id='listing-section-ajax-holder'>
	<?php
	kb_generate_listings('search');
	?>
</div>

<?php  
get_footer();
?>
