<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bluestag
 */
 
 
	// OUTPUT THE FOOTER IF WE ARE NOT ON THE 404 PAGE
  if(!is_404()){get_template_part( 'parts/common/ui', 'footer' );}

		 

  wp_footer(); ?>

</body>
</html>
