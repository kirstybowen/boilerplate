<?php

	/////////////////////
	// ENQUEUE SCRIPTS //
	/////////////////////
	function site_scripts(){

		// REGISTER JS
		wp_deregister_script('jquery');
		wp_register_script('jquery', get_template_directory_uri() . '/assets/js/jquery.3.6.min.js' , array(), '3.6' );
		wp_register_script('main',get_template_directory_uri() . '/app.min.js' , array('jquery') , filemtime(get_template_directory().'/app.min.js'),true);
		wp_register_script('kb',get_template_directory_uri() . '/assets/src/js/kb.js' , array('jquery') , filemtime(get_template_directory().'/assets/src/js/kb.js'),true);
		wp_localize_script('kb', 'ajax_object', [ 'ajax_url' => admin_url( 'admin-ajax.php' ) ] );

		// ENABLE OR REMOVE
		// wp_register_script('lottie',get_template_directory_uri() . '/assets/js/jquery.lottie.min.js' , array('jquery') , filemtime(get_template_directory().'/app.min.js'),true);
		// wp_register_script('plyr',get_template_directory_uri() . '/assets/js/jquery.plyr.min.js' , array('jquery') , filemtime(get_template_directory().'/assets/js/jquery.plyr.min.js'),true);

		// ENQUEUE JS
		wp_enqueue_script('main');
		wp_enqueue_script('kb');
		// wp_enqueue_script('lottie');
		// wp_enqueue_script('plyr');

		// ENQUEUE CSS
		wp_enqueue_style('main',get_template_directory_uri() . "/style.min.css",array(),filemtime(get_template_directory()."/style.min.css"));
		wp_dequeue_style( 'wp-block-library' );

	} add_action( 'wp_enqueue_scripts', 'site_scripts' );
 
?>