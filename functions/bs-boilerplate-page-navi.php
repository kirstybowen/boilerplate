<?php // Borrowed with love from FoundationPress
	function joints_page_navi() {
		global $wp_query;
		$big = 999999999; // This needs to be an unlikely integer
		// For more options and info view the docs for paginate_links()
		// http://codex.wordpress.org/Function_Reference/paginate_links
		$paginate_links = paginate_links( array(
			'base' => str_replace( $big, '%#%', html_entity_decode( get_pagenum_link( $big ) ) ),
			'current' => max( 1, get_query_var( 'paged' ) ),
			'total' => $wp_query->max_num_pages,
			'mid_size' => 5,
			'prev_next' => true,
		    'prev_text' => __( '&laquo;', 'jointswp' ),
		    'next_text' => __( '&raquo;', 'jointswp' ),
			'type' => 'list',
		) );
		$paginate_links = str_replace( "<ul class='page-numbers'>", "<ul>", $paginate_links );
		$paginate_links = str_replace( '<li><span class="page-numbers dots">', "<li><a href='#'>", $paginate_links );
		$paginate_links = str_replace( "<li><span class='page-numbers current'>", "<li class='current'>", $paginate_links );
		$paginate_links = str_replace( '</span>', '</a>', $paginate_links );
		$paginate_links = str_replace( "<li><a href='#'>&hellip;</a></li>", "<li><span class='dots'>&hellip;</span></li>", $paginate_links );
		$paginate_links = preg_replace( '/\s*page-numbers/', '', $paginate_links );
		// Display the pagination if more than one page is found.
		if ( $paginate_links ) {
			echo '<nav class="pagination">';
			echo $paginate_links;
			echo '</nav>';
		}
	}



	function joints_page_navi_ajax($page_id, $paged, $max_num_pages,$pagination_args){
		$big = 999999999;		
    $page_url = str_replace( home_url(), "", get_permalink( $page_id ) );
		?>

    <nav class="pagination" data-base-url="<?php echo $page_url; ?>">
        <?php echo paginate_links( array(
            'base'      => str_replace( $big, '%#%', esc_url( kb_get_pagenum_link( $big, true, $page_url ) ) ),
            'format'    => '?paged=%#%',
            'current'   => max( 1, $paged ),
            'total'     => $max_num_pages,
            'prev_text' => __( 'Previous', 'arkay' ),
            'next_text' => __( 'Next', 'arkay' ),
            'type'      => 'list',
            'end_size'  => 1,
            'mid_size'  => 1,
            'add_args'  => $pagination_args,
        ) ); ?>
    </nav>


<?php
	}