<?php




    ///////////////////////////////////////////////////
    // OUTPUTS BANNERS
    ///////////////////////////////////////////////////
function kb_generate_banner($type='default'){
 
get_template_part('parts/banners/content-banner',$type);
}


    ///////////////////////////////////////////////////
    // OUTPUTS TERMS
    ///////////////////////////////////////////////////
function kb_get_the_terms($id,$taxonomy){


    $terms = wp_get_post_terms($id, $taxonomy,  array("fields" => "names"));

    $output = "";
    if(is_array($terms)){

        foreach ( $terms  as $key=>$term ) { 

          $output .=$term. ", ";
      }
  }
  $output = rtrim($output,', ');
  return $output;
}



    ///////////////////////////////////////////////////
    // OUTPUTS CATEGORIES
    ///////////////////////////////////////////////////
function kb_get_the_cats($cats){



    $output = "";
    if(is_array($cats)){

        foreach ( $cats  as $category ) { 

          $output .=$category->name. ", ";
      }
  }
  $output = rtrim($output,', ');
  return $output;
}

  
 




 







// output the thumbnail image, and optionally a placeholder if it doesn't have one set
    function bs_the_post_thumbnail($size, $placeholder=false, $id=false){ 

        global $post;

        if($id == false){
            $id = get_the_id();
        }

// OUTPUTS THE FEATURED IMAGE
        if ( has_post_thumbnail($id) ) :  
            echo get_the_post_thumbnail($id, $size);     
        else:

// OUTPUT THE PLACEHOLDER IF REQUIRED
            if($placeholder == true){ 
                $banner_fallback = get_field('placeholder_image', 'option');   

                echo wp_get_attachment_image( $banner_fallback['ID'], $size );

            }
        endif;


    }







// output the ACF  image, and optionally a placeholder if it doesn't have one set
    function output_bs_the_acf_thumbnail($image, $size, $placeholder=false, $nolazy=false){ 


        $image_id = $image['ID']; 
        if( $image_id ):

           echo wp_get_attachment_image( $image_id, $size, "", ["class" => "".$nolazy.""] ); 


                            // echo $image['sizes'][$size];  //if you need just the url


       else: 
    // OUTPUT THE PLACEHOLDER IF REQUIRED
        if($placeholder == true){ 
            $banner_fallback = get_field('placeholder_image', 'option');  
            echo wp_get_attachment_image( $banner_fallback['ID'], $size );

        }
    endif;


}


// output the ACF  image url, and optionally a placeholder if it doesn't have one set
function output_bs_the_acf_thumbnail_url($image, $size, $placeholder=false){ 
    $return = false;

    $image_id = $image['ID']; 
    if( $image_id ):  
    $return =  $image['sizes'][$size];  //if you need just the url  
else: 
    // OUTPUT THE PLACEHOLDER IF REQUIRED
    if($placeholder == true){ 
        $banner_fallback = get_field('placeholder_image', 'option');   
        $return = $banner_fallback['sizes'][$size];
    }
endif;
return $return;

}




function bs_the_post_thumbnail_url($this_id, $size, $placeholder=false){ 
    global $post;
    $return = false;

// OUTPUTS THE FEATURED IMAGE
    if ( has_post_thumbnail($this_id) ) :  
        $return = get_the_post_thumbnail_url($this_id,$size); 
    else:

// OUTPUT THE PLACEHOLDER IF REQUIRED
        if($placeholder == true){ 
            $banner_fallback = get_field('placeholder_image', 'option');  
            $return = $banner_fallback['sizes'][$size];
        }
    endif;
    return $return;


}








?>