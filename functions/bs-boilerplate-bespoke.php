<?php
// THIS IS ANY BESPOKE FUNCTIONS BY BLUE STAG



 


function kb_not_password(){ 
   if(is_home()){
        $location = get_option('page_for_posts', true);
    } else{
    	$location = "";
    }
	if ( post_password_required($location) ) { echo get_the_password_form(); return false; }else{
		return true;
	}
}



//LETS DISBALE ALL COMMENTS

// Add to existing function.php file
// Disable support for comments and trackbacks in post types
function df_disable_comments_post_types_support() {
	$post_types = get_post_types();
	foreach ($post_types as $post_type) {
		if(post_type_supports($post_type, 'comments')) {
			remove_post_type_support($post_type, 'comments');
			remove_post_type_support($post_type, 'trackbacks');
		}
	}
}
add_action('admin_init', 'df_disable_comments_post_types_support');
// Close comments on the front-end
function df_disable_comments_status() {
	return false;
}
add_filter('comments_open', 'df_disable_comments_status', 20, 2);
add_filter('pings_open', 'df_disable_comments_status', 20, 2);
// Hide existing comments
function df_disable_comments_hide_existing_comments($comments) {
	$comments = array();
	return $comments;
}
add_filter('comments_array', 'df_disable_comments_hide_existing_comments', 10, 2);
// Remove comments page in menu
function df_disable_comments_admin_menu() {
	remove_menu_page('edit-comments.php');
}
add_action('admin_menu', 'df_disable_comments_admin_menu');
// Redirect any user trying to access comments page
function df_disable_comments_admin_menu_redirect() {
	global $pagenow;
	if ($pagenow === 'edit-comments.php') {
		wp_redirect(admin_url()); exit;
	}
}
add_action('admin_init', 'df_disable_comments_admin_menu_redirect');
// Remove comments metabox from dashboard
function df_disable_comments_dashboard() {
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
}
add_action('admin_init', 'df_disable_comments_dashboard');
// Remove comments links from admin bar
function df_disable_comments_admin_bar() {
	if (is_admin_bar_showing()) {
		remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
	}
}
add_action('init', 'df_disable_comments_admin_bar');








function kb_get_page_by_template($template = '') {


    $args = array(
        'meta_key' => '_wp_page_template',
        'meta_value' => $template
    );
    return get_pages($args); 
}



    ////////////////////////////////////
    // STANDARD LINK HELPER
    ////////////////////////////////////
function bs_convert_link($the_link){

    if(isset($the_link['url'])){
        $link = array();    
        $link['url'] = $the_link['url'];
        $link['target'] =  '_self';
        $link['title'] =  'Learn More';
        if(isset($the_link['title'])&& !empty($the_link['title'])){$link['title']  =  $the_link['title']; }
        if(isset($the_link['target']) && !empty($the_link['target'])){$link['target']  =  $the_link['target']; }


    }else{
        $link = false;
    }


    return $link;
}




// JUST A HELPER FOR DEUBUGING ARRAYS

function bs_print($print){
    echo "<pre>";
    print_r($print);
    echo "</pre>";
}


//  HELPER TO LOOK THROUGH FLEXIBLE CONTENT BLOCKS
function bs_flex_master(){ 
    $blocks_location  = '';

//if on news archive
    if(is_home()){
        $blocks_location = get_option('page_for_posts', true);
    }else if(is_tax()){

//if on taxonomy
        $blocks_location  = get_queried_object();
    }

// if password protected
    if ( post_password_required($blocks_location) ) {
// SHOW THE FORM
        echo get_the_password_form();
    } else {  
        if ( have_rows( 'page_blocks'  ,$blocks_location) ): 

// LOOP THROUGH BLOCKS
            while ( have_rows( 'page_blocks' ,$blocks_location) ) : the_row();
               $template = str_replace('layout_','',get_row_layout());

               get_template_part( 'parts/flex/block', $template );  

               wp_reset_query();
               wp_reset_postdata();

           endwhile; 

       endif;

   }


}


/**
 * AJAX - filter and retrieve case study output
 */

function kb_listing_filter() {
    kb_generate_listings();

    die();
}

add_action('wp_ajax_kb_listing_filter', __NAMESPACE__ . '\\kb_listing_filter');
add_action('wp_ajax_nopriv_kb_listing_filter', __NAMESPACE__ . '\\kb_listing_filter');




function kb_get_block_group_block_intro($intro){
 
if($intro['enabled'] != 1){ 
$intro['title'] = ''; 
$intro['intro_text'] = '';
} 
$intro = array('enabled'=>$intro['enabled'],'title'=>$intro['title'],'intro_text'=>$intro['intro_text']);
 
return $intro;
}



function kb_get_pagenum_link( $pagenum = 1, $escape = true, $base = null ) {
	global $wp_rewrite;

	$pagenum = (int) $pagenum;

	$request = $base ? remove_query_arg( 'paged', $base ) : remove_query_arg( 'paged' );

	$home_root = parse_url( home_url() );
	$home_root = ( isset( $home_root['path'] ) ) ? $home_root['path'] : '';
	$home_root = preg_quote( $home_root, '|' );

	$request = preg_replace( '|^' . $home_root . '|i', '', $request );
	$request = preg_replace( '|^/+|', '', $request );

	if ( ! $wp_rewrite->using_permalinks() || is_admin() ) {

		$base = trailingslashit( get_bloginfo( 'url' ) );

		if ( $pagenum > 1 ) {
			$result = add_query_arg( 'paged', $pagenum, $base . $request );
		} else {
			$result = $base . $request;
		}
	} else {
		$qs_regex = '|\?.*?$|';
		preg_match( $qs_regex, $request, $qs_match );

		if ( ! empty( $qs_match[0] ) ) {
			$query_string = $qs_match[0];
			$request      = preg_replace( $qs_regex, '', $request );
		} else {
			$query_string = '';
		}

		$request = preg_replace( "|$wp_rewrite->pagination_base/\d+/?$|", '', $request );
		$request = preg_replace( '|^' . preg_quote( $wp_rewrite->index, '|' ) . '|i', '', $request );
		$request = ltrim( $request, '/' );

		$base = trailingslashit( get_bloginfo( 'url' ) );

		if ( $wp_rewrite->using_index_permalinks() && ( $pagenum > 1 || '' !== $request ) ) {
			$base .= $wp_rewrite->index . '/';
		}

		if ( $pagenum > 1 ) {
			$request = ( ( ! empty( $request ) ) ? trailingslashit( $request ) : $request ) . user_trailingslashit( $wp_rewrite->pagination_base . '/' . $pagenum, 'paged' );
		}

		$result = $base . $request . $query_string;
	}


    /**
     * Filters the page number link for the current request.
     *
     * @since 2.5.0
     * @since 5.2.0 Added the `$pagenum` argument.
     *
     * @param string $result  The page number link.
     * @param int    $pagenum The page number.
     */
    $result = apply_filters( 'get_pagenum_link', $result, $pagenum );


    if ( $escape ) {
    	return esc_url( $result );
    } else {
    	return esc_url_raw( $result );
    }
}


function kb_get_search_post_types(){
	return array('post', 'page');
}
 


// redirect 
/* Disable archives pages */
// add_action('template_redirect', 'my_disable_archives_function');

// function my_disable_archives_function()
// {
//    Conditional checks examples:
//       is_category() 
//       is_tag()
//       is_date()
//       is_author()
//       is_tax()
//       is_search() ... 


//   if ( is_category()  || is_author()  || is_tag()  )
//   {
//       global $wp_query;
//       $wp_query->set_404();
//   }
// }





// function wpse121723_register_sidebars() {
//     register_sidebar( array(
//         'name' => 'After News Item',
//         'id' => 'kb_widget_after_single',
//         'before_widget' => '<div>',
//         'after_widget' => '</div>',
//         'before_title' => '<h2 class="rounded">',
//         'after_title' => '</h2>',
//     ) );
// }
// add_action( 'widgets_init', 'wpse121723_register_sidebars' );






// function wpdocs_allowed_post_type_blocks( $allowed_block_types, $editor_context ) {
// 	 	return array(
// 			'core/paragraph',
// 		);


// 	return $blocks;
// }

// add_filter( 'allowed_block_types_all', 'wpdocs_allowed_post_type_blocks', 10, 2 );
 
/**
 * Show the banner when a html element with class 'cmplz-show-banner' is clicked
 */
function cmplz_show_banner_on_click() {
	?>
	<script>
        function addEvent(event, selector, callback, context) {
            document.addEventListener(event, e => {
                if ( e.target.closest(selector) ) {
                    callback(e);
                }
            });
        }
        addEvent('click', '.cmplz-show-banner', function(){
            document.querySelectorAll('.cmplz-manage-consent').forEach(obj => {
                obj.click();
            });
        });
	</script>
	<?php
}
add_action( 'wp_footer', 'cmplz_show_banner_on_click' );



?>