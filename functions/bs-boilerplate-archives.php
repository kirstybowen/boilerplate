<?php



    ///////////////////////////////////////////////////
    // OUTPUTS ARCHIVE FILTERS
    ///////////////////////////////////////////////////

function kb_generate_filters($type='post'){

	get_template_part('parts/filters/filter',$type);


}






    ///////////////////////////////////////////////////
    // DETERMINES ARCHIVE LISTINGS FUNCTION TO USE
    ///////////////////////////////////////////////////
function kb_generate_listings($post_type=false, $page_id=false) {


 // if we are calling the function via ajax then get the page_id and post_type from $_POST
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		$page_id = $_POST['page_id'];
		$post_type = $_POST['post_type'];
	}


    // generate the listings dependant on the post type
	if($post_type == 'post'){
		kb_generate_listings_post($post_type, $page_id);
	}else if($post_type == 'search'){ 
		kb_generate_listings_search($post_type, $page_id);
	}else if($post_type == 'kb-case-study'){ 
		kb_generate_listings_case_study($post_type, $page_id);
	}

}





    ///////////////////////////////////////////////////
    // ARCHIVE-POSTS
    ///////////////////////////////////////////////////
function kb_generate_listings_post($post_type=false, $page_id=false){

	$paged = 1;

	if ( get_query_var('paged') ) $paged = get_query_var( 'paged' );
	if ( get_query_var('page') ) $paged = get_query_var( 'page' );


	$args = [
		'post_type'      => $post_type,
		'posts_per_page' => 1, 
		'post_status'    => 'publish',
		'orderby'        => 'post_date',
		'order'          => 'DESC',
		'paged'          => $paged,
	];

	$pagination_args = [];


	// filter by category
	if ( true === array_key_exists( 'cat', $_REQUEST ) && false === empty( $_REQUEST['cat'] ) ) {
		$args['tax_query'][] = [
			[
				'taxonomy' => 'category',
				'field'    => 'term_id',
				'terms'    => [ $_REQUEST['cat'] ],
			],
		];

		$pagination_args['cat'] = $_REQUEST['cat'];
	}


	$listings = new \WP_Query( $args );

	if ( $listings->have_posts() ) :
		?>
		<section class="flex archive <?php echo $post_type; ?>">
			<g-container> 
				<g-row>
					<g-col>
						<post-list> 
							<?php
							while ( $listings->have_posts() ) {
								$listings->the_post();
								get_template_part('parts/tiles/tile',$post_type, [ 'post_id' => get_the_id() ]); 
							}
							?>

						</post-list>
					</g-col>
				</g-row>
			</g-container>
		</section>
		<?php 
		joints_page_navi_ajax($page_id, $paged, $listings->max_num_pages,$pagination_args);
		wp_reset_postdata();
		?>
		<?php else : ?>
			<p><?php echo  'No entries were found' ; ?></p>
			<?php
		endif;
	}





    ///////////////////////////////////////////////////
    // ARCHIVE-CASE STUDIES
    ///////////////////////////////////////////////////
function kb_generate_listings_case_study($post_type=false, $page_id=false){

	$paged = 1;

	if ( get_query_var('paged') ) $paged = get_query_var( 'paged' );
	if ( get_query_var('page') ) $paged = get_query_var( 'page' );


	$args = [
		'post_type'      => $post_type,
		'posts_per_page' => 12, 
		'post_status'    => 'publish',
		'orderby'        => 'post_date',
		'order'          => 'DESC',
		'paged'          => $paged,
	];

	$pagination_args = [];


	// // filter by category
	// if ( true === array_key_exists( 'cat', $_REQUEST ) && false === empty( $_REQUEST['cat'] ) ) {
	// 	$args['tax_query'][] = [
	// 		[
	// 			'taxonomy' => 'category',
	// 			'field'    => 'term_id',
	// 			'terms'    => [ $_REQUEST['cat'] ],
	// 		],
	// 	];

	// 	$pagination_args['cat'] = $_REQUEST['cat'];
	// }


	$listings = new \WP_Query( $args );

	if ( $listings->have_posts() ) :
		?>
		<section class="flex archive <?php echo $post_type; ?>">
			<g-container> 
				<g-row>
					<g-col>
						<post-list> 
							<?php
							while ( $listings->have_posts() ) {
								$listings->the_post();
								get_template_part('parts/tiles/tile',$post_type, [ 'post_id' => get_the_id() ]); 
							}
							?>

						</post-list>
					</g-col>
				</g-row>
			</g-container>
		</section>
		<?php 
		joints_page_navi_ajax($page_id, $paged, $listings->max_num_pages,$pagination_args);
		wp_reset_postdata();
		?>
		<?php else : ?>
			<p><?php echo  'No entries were found' ; ?></p>
			<?php
		endif;
	}



    ///////////////////////////////////////////////////
    // ARCHIVE-SEARCH
    ///////////////////////////////////////////////////
	function kb_generate_listings_search($post_type=false){

		$page_id = 'search';

		$paged = 1;

		if ( get_query_var('paged') ) $paged = get_query_var( 'paged' );
		if ( get_query_var('page') ) $paged = get_query_var( 'page' );




		$args = [ 
			's' => get_search_query(false),
			'posts_per_page' => 2,  
			'post_status'	=>'publish',
			'paged'          => $paged, 
			'post_type'		 => kb_get_search_post_types()
		];
     
		$pagination_args = [];


	// filter by content_type
		if ( true === array_key_exists( 'content_type', $_REQUEST ) && false === empty( $_REQUEST['content_type'] ) ) {
			$args['post_type']  =$_REQUEST['content_type'];   
			$pagination_args['content_type'] = $_REQUEST['content_type'];
		} 

 
		$listings = new \WP_Query( $args );

		if ( $listings->have_posts() ) :
			?>
			<section class="flex archive <?php echo $post_type; ?>">
				<g-container> 
					<g-row>
						<g-col>
							<post-list> 
								<?php
								while ( $listings->have_posts() ) {
									$listings->the_post();
									get_template_part('parts/tiles/tile',$post_type, [ 'post_id' => get_the_id() ]); 
								}
								?>

							</post-list>
						</g-col>
					</g-row>
				</g-container>
			</section>
			<?php 
			joints_page_navi_ajax($page_id, $paged, $listings->max_num_pages,$pagination_args);
			wp_reset_postdata();
			?>
			<?php else : ?>
				<p><?php echo  'No entries were found' ; ?></p>
				<?php
			endif;
		}





		?>