<?php


 // THE TAXONOMY
$cats = get_terms( 'category', [
  'hide_empty' => true,
] );



// check for active filtet
$cat_value = ''; 
if ( true === array_key_exists( 'cat', $_REQUEST ) && false === empty( $_REQUEST['cat'] ) ) {
  $cat_value = $_REQUEST['cat'];
}

 
 
?>


<div id="header-filter">
  <div class="item item-kb_cat">
    <label for="filter-kb_cat">Category</label>
    <select data-filter-type='cat' name="filter-kb_cat" id="filter-kb_cat" class="form-control">
      <option value="">All</option>
      <?php
      foreach ( $cats as $item ): ?>
        <option value="<?php echo $item->term_id ?>" <?php if ( $item->term_id == $cat_value ) echo ' selected="selected"'; ?>><?php echo $item->name; ?></option>
      <?php  endforeach ?>
    </select>
  </div>

  
</div>
<input type='hidden' id='kb_page_id' value='<?php echo get_option( 'page_for_posts' ); ?>'/>
<input type='hidden' id='kb_post_type' value='post'/>

