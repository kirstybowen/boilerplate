<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bluestag
 */

?>

<div class="post-not-found">
 
	<?php if ( is_search() ) : ?>
		
 
		<section class="entry-content">
			<p><?php _e( 'Try your search again.', 'bluestag' );?></p>
		</section>
		 
	 
		
	<?php else: ?>
	
		<section class="entry-content">
			<p><?php _e( 'Oops, Nothing Found', 'bluestag' ); ?></p>
		</section>


			
	<?php endif; ?>
	
</div>


 
