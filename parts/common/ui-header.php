<?php
$site_notice_enabled= get_field('site_notice_enabled','options');

$site_notice = get_field('site_notice','options');
if($site_notice_enabled == true){

	echo $site_notice;
}

?>
		<!-- HEADER -->
		<header>
			<header-inner>

				<a class="logo" href="<?php echo get_home_url() ?>">
					{LOGO}
				</a>
<?php

	// MAIN MENU
	joints_top_nav();

?> 

			</header-inner>



		</header>


<!-- MOBILE NAV TRIGGER -->
		<button data-action="toggle-nav" type="button" aria-label="Open menu">
			<button-inner></button-inner>
		</button>
