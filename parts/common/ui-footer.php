		<!-- FOOTER -->
		<footer>
			<g-container>
				<g-row>
					<g-col>

						<!-- QUICK LINKS -->  
						<strong class="h4">Useful links</strong>
						<?php joints_footer_links('column'); ?>

					</g-col>
					<g-col>

						<!-- SOCIAL -->
						<strong class="h4">Follow us</strong>
						<?php set_query_var('social_links', get_field('social_links','options')); ?> 
						<?php get_template_part('parts/common/ui','social'); ?>

						<!-- EMAIL -->
						<strong class="h4">Drop us an email</strong>
						<?php echo get_field('email_address','options'); ?>

						<!-- PHONE -->
						<strong class="h4">Give us call</strong>
						<?php echo get_field('tel_number','options'); ?>


					</g-col>

				</g-row>
	
				<g-row>
					<g-col>
						<!-- FOOTER NOTICE -->
						<?php echo get_field('footer_notice','options'); ?>

						<?php joints_footer_links('legal'); ?>
					</g-col>
					<g-col>
						<a href="https://bluestag.co.uk/" rel="noopener" target="_blank">By Blue Stag</a>
					</g-col>
				</g-row>

			</g-container>
		</footer>

		<mobile-nav data-url="<?php echo get_home_url() ?>">
			<nav-inner>
				
				<a href="<?php echo get_home_url() ?>">
					<svg width="148" height="160" viewBox="0 0 148 160" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M77.8154 57.1953C75.1333 47.8502 71.359 40.1952 59.3098 32.1396C53.2836 28.1109 45.1049 27.1084 35.9713 27.569C22.3741 28.2609 6.12467 34.5477 1.29625 50.1178C-4.00677 67.2103 8.20104 78.2859 15.5511 81.8503C28.7562 88.9859 45.1561 86.4696 61.694 79.1509C62.1461 73.7134 61.9987 69.0396 61.2481 65.229C48.447 71.6546 32.7715 76.6071 21.561 70.474C11.364 66.5896 5.38217 40.7277 36.6245 39.2509C55.5111 38.3584 61.6571 50.8459 64.2375 59.2634H64.2381C64.24 59.2709 64.2418 59.2778 64.2443 59.2853C64.6777 60.7034 65.0137 62.0084 65.2891 63.1103C66.4981 67.9703 66.618 73.9228 65.9698 80.474C65.9591 80.5821 65.9535 80.6846 65.9429 80.7934L65.9379 80.7928C62.9954 109.518 45.4402 149.548 39.8318 159.804H65.8761C69.1683 141.838 84.9706 82.1278 77.8154 57.1953Z"/>
						<path d="M147.391 0.006875L147.331 0.06L147.384 0C147.384 0 120.822 19.3288 99.3666 16.255C99.3666 16.255 101.169 23.8913 115.74 27.9481L115.782 27.9913C105.498 38.0094 92.9789 46.47 81.759 53.91C81.98 54.6075 82.1992 55.3288 82.4159 56.0856C83.2627 59.0356 83.8347 62.4769 84.1445 66.4138C98.2419 56.1688 109.17 41.9163 118.883 31.095L119.466 31.6781C123.519 46.2606 131.15 48.0644 131.15 48.0644C128.079 26.5919 147.391 0.006875 147.391 0.006875Z"/>
					</svg>
				</a>

			</nav-inner>
		</mobile-nav>

		
<p>Manage your cookie preferences <a class="cmplz-show-banner">by clicking here.</a></p>