	<!-- ERROR 404 -->
	<section class="error-404">
		<g-container>
			<g-row>
				<g-col>

					<!-- MESSAGE -->
					<h1>Error 404</h1>
					<p>We're sorry, but the page you were looking for could not be found.</p>
					<a href="<?php echo get_home_url(); ?>" class="btn red toBlack fill rounded">Return to the Homepage</a>
  
				</g-col>
			</g-row>
		</g-container>
	</section>