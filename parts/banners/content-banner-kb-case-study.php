
	<!-- SINGLE NEWS -->
	<section class="news hero">
		<g-container>
			<g-row>
				<g-col> 
			 
					<h1><?php the_title(); ?></h1>  
				</g-col>
			</g-row>
<?php if ( has_post_thumbnail() ) { ?>
			<g-row>
				<g-col>
					<figure><?php bs_the_post_thumbnail('large',false); ?></figure>
				</g-col>
			</g-row>
<?php } ?>
		</g-container>
	</section>