
<?php get_search_form(['prefill'=>true]); ?><?php

	////////////////////////////
	// THE SEARCJ PAGE BANNER //
	//////////////////////////// 


$image = "";

?>

<!-- BANNER -->
<section class="flex np search hero">
	<g-container> 
		<g-row>
			<g-col>
				<h1>

					<?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Search Results for: %s', 'bluestag' ), '<span>' . get_search_query() . '</span>' );
					?>
					
<?php get_search_form(['prefill'=>true]); ?>



				</h1> 
			</g-col>
		</g-row>
	</g-container>
	<?php if ( $image ) { ?>
		<figure>
			<?php echo output_bs_the_acf_thumbnail($image, 'large', true); ?>
		</figure>
	<?php } ?>
</section>

