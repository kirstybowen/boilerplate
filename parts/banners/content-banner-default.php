<?php

	////////////////////////////
	// THE COMMON PAGE BANNER //
	////////////////////////////
	$location  = '';

	// THE CURRENT PAGE'S URL
	$current_url = get_permalink();

	// IF ON NEWS ARCHIVE
	if ( is_home() ) {
		$location = get_option('page_for_posts', true);
		$current_url = get_post_type_archive_link('post');
	} 

	// THE HERO
	$hero = get_field('block_group_hero', $location);

	// TITLE
	$hero_title = $hero['title'];
	$hero_text = $hero['text'];

	// DEFAULT TO PAGE TITLE IF ONE ISN'T SET
	if ( empty($hero_title) ) { $hero_title = get_the_title($location); }

	// IF ON AN ARCHIVE 
	if (is_archive() ) { $hero_title = get_the_archive_title(); }

?>

	<!-- BANNER -->
	<section class="flex np page hero">
		<g-container> 
			<g-row>
				<g-col>
					<h1><?php echo $hero_title; ?></h1>
					<?php if(!empty($hero_text)){ ?>
							<p><?php echo $hero_text; ?></p>
					<?php  } ?>
				</g-col>
			</g-row>
		</g-container>
<?php if ( $hero['image'] ) { ?>
		<figure>
			<?php echo output_bs_the_acf_thumbnail($hero['image'], 'large', true); ?>
		</figure>
<?php } ?>
	</section>
