
	<!-- SINGLE NEWS -->
	<section class="news hero">
		<g-container>
			<g-row>
				<g-col>

					<?php echo kb_get_the_cats(get_the_category());?>
			 
					<h1><?php the_title(); ?></h1> 
					<span>
						<strong>By <?php _e(get_the_author()); ?></strong>
						<time>Posted <?php the_time('jS F Y'); ?></date>
					</span>
				</g-col>
			</g-row>
<?php if ( has_post_thumbnail() ) { ?>
			<g-row>
				<g-col>
					<figure><?php bs_the_post_thumbnail('large',false); ?></figure>
				</g-col>
			</g-row>
<?php } ?>
		</g-container>
	</section>