<?php

	////////////////////////
	// BLOCK: TEXT EDITOR //
	////////////////////////

 
  $groupName = 'block_group_text_editor';

if ( have_rows( $groupName ) ) { while ( have_rows( $groupName) ) { the_row();

	$content =  get_sub_field('content');  
 
 
}
}else{ 
	extract($args);
}

	 ?>
	<section class="flex text wysiwyg">
		<g-container>
			<g-row>
				<g-col>
					<?php echo $content; ?>
				</g-col>
			</g-row>
		</g-container>
	</section>
