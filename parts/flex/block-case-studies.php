<?php

	////////////////////////
	// BLOCK: Case Studies (latest or selected) //
	////////////////////////
 $groupName = 'block_group_case_studies';
$output = [];


$post_type = 'kb-case-study';

if ( have_rows( $groupName ) ) { 
	while ( have_rows( $groupName) ) {
		the_row();

		$output_type =  get_sub_field('output_type');  //latest_3, latest_6, selected
		$intro = get_sub_field('block_intro');


		// IF LATEST NEWS
		if(substr($output_type,0,7) == 'latest_')
		{

			$count = str_replace('latest_', '', $output_type);

       // query latest news and add ID's to $output array
			$latest_args  = [
				'post_type'      =>  $post_type,
				'posts_per_page' => $count, 
				'post_status'    => 'publish',
				'orderby'        => 'post_date',
				'order'          => 'DESC', 
			]; 


			$listings = new \WP_Query( $latest_args );

			if ( $listings->have_posts() ) {

				while ( $listings->have_posts() ) { $listings->the_post();

					$output[] = get_the_id();

				}

				wp_reset_postdata();

			}
		}else if($output_type == 'selected'){

		// select using ACF post object field
			$select_items = get_sub_field('select_items');


			if(is_array($select_items)){

				foreach ($select_items as $key=>$value) {

					$output[] = $value;

				}

			}






		}
	}
}else if(isset($args['related'])){

	// block being called from a single post (related)
	$intro = ['intro_title'=>'Continue Reading...']; 
	$latest_args  = [
		'post_type'      => $post_type,
		'posts_per_page' => 3, 
		'post_status'    => 'publish',
		'orderby'        => 'post_date',
		'order'          => 'DESC', 
		'post__not_in' =>[$args['current_post']]
	]; 


	$listings = new \WP_Query( $latest_args );

	if ( $listings->have_posts() ) {

		while ( $listings->have_posts() ) { $listings->the_post();

			$output[] = get_the_id();

		}

		wp_reset_postdata();

	} 
}




// START OF OUTPUT 
if(is_array($output) && count($output) > 0){
	?> 
	<section class="flex archive <?php echo $post_type; ?> other-posts">
		<g-container>
			<g-row>
				<g-col>
					<heading-wrap>
						<?php if(!empty($intro['intro_title'])){ ?><h3><?php echo $intro['intro_title']; ?></h3><?php } ?> 
						<?php if(!empty($intro['intro_text'])){ ?><p><?php echo $intro['intro_text']; ?></p><?php } ?>

					</heading-wrap> 
					<post-list>
						<?php 
						foreach ( $output as $key => $value) {   
							get_template_part( 'parts/tiles/tile',$post_type , array('post_id'=>$value));  
						} 
						?>
					</post-list>

				</g-col>
			</g-row>
		</g-container>
	</section> 
	<?php 

}
?>