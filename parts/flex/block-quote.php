<?php

	////////////////////////
	// BLOCK: QUOTE //
	////////////////////////
$groupName = 'block_group_quote';

if ( have_rows( $groupName ) ) : while ( have_rows( $groupName) ) : the_row();

	// FIELDS
	$text =  get_sub_field('quote_text'); 
	$name =  get_sub_field('name'); 
	$title =  get_sub_field('title'); 
	$image = get_sub_field('image'); 
	?>
	
	<section class="flex text wysiwyg">
		<g-container>
			<g-row>
				<g-col>
					<?php echo $text; ?>
					<p><?php echo $name; ?></p>
					<?php if(!empty($title)){ ?><p><?php echo $title; ?></p><?php } ?>

					<?php output_bs_the_acf_thumbnail($image, 'thumbnail')?>
				</g-col>
			</g-row>
		</g-container>
	</section>

	<?php endwhile; endif; ?>