<?php
$reusable = get_sub_field('block_reusable'); 
if ( have_rows( 'page_blocks'  ,$reusable) ):  
// LOOP THROUGH BLOCKS
	while ( have_rows( 'page_blocks' ,$reusable) ) : the_row();
		$template = str_replace('layout_','',get_row_layout()); 
		get_template_part( 'parts/flex/block', $template );   

	endwhile; 

endif;


?>