<?php

	//////////////////////////
	// BLOCK: IMAGE GALLERY //
	//////////////////////////
	$groupName = 'block_group_gallery';

	// IF HAVE ROWS  
	if ( have_rows( $groupName ) ) : while ( have_rows( $groupName) ) : the_row();

		// THE CONTENT
		$images = get_sub_field('images');

		// IMAGE SIZE
		$size = 'large';

		// IF HAVE IMAGES
		if ( $images ) :

?>

	<!-- BLOCK: GALLERY -->
	<section class="flex gallery">
		<div class="slideshow-container">

<?php

	// OUTPUT EACH IMAGE
	foreach( $images as $image ) {

		echo "<div class=\"slide\"><figure>";
		output_bs_the_acf_thumbnail($image, $size, false, 'nolazy');  
		echo "</figure></div>";

	}

?> 

		</div>
		<div class="dots"></div>
	</section>

<?php

		// END IF
		endif;

	// END
	endwhile; endif;

?>