<?php

	////////////////////////////
	// BLOCK: TEXT WITH MEDIA //
	////////////////////////////
$groupName = 'block_group_text_with_media';

	// IF HAS ROWS
if ( have_rows( $groupName ) ) :  while ( have_rows( $groupName) ) : the_row();

		// THE LINK

	// FIELDS
	$title =  get_sub_field('title'); 
	$text =  get_sub_field('text'); 
	$button = bs_convert_link(get_sub_field('button'));
	$position = get_sub_field('position');
	$image = get_sub_field('image');
	?> 

	<!-- BLOCK: TEXT WITH MEDIA -->
	<section class="flex text-with-media <?php echo $position; ?>">
		<g-container>
			<g-row>
				<g-col class="media">
					<?php output_bs_the_acf_thumbnail($image, 'medium', false); ?>
				</g-col>
				<g-col class="wysiwyg">
					<?php if(!empty($title)){
						echo "<h2>".$title."</h2>";
					} ?>
					<?php echo $text; ?>
					<?php  if ( $button !== false ) { echo "\t\t\t\t\t\t\t<a class=\"btn fill red toBlack\" href=\"" . $button['url'] . "\" target=\"" . $button['target'] . "\">" . $button['title'] . "</a>"; } ?>
				</g-col>
			</g-row>
		</g-container>
	</section>

	<?php endwhile; endif; ?>