<?php

	////////////////////////
	// BLOCK: KEY STATEMENT //
	////////////////////////
$groupName = 'block_group_key_statement';

if ( have_rows( $groupName ) ) : while ( have_rows( $groupName) ) : the_row();

	// FIELDS
	$text =  get_sub_field('text'); 
	?>
 
	<section class="flex text wysiwyg">
		<g-container>
			<g-row>
				<g-col>
					<?php echo $text; ?>
				</g-col>
			</g-row>
		</g-container>
	</section>

	<?php endwhile; endif; ?>