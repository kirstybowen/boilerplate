<?php

	////////////////////////
	// BLOCK: Embed //
	////////////////////////
	$groupName = 'block_group_embed';

	if ( have_rows( $groupName ) ) : while ( have_rows( $groupName) ) : the_row();

?>

	<!-- BLOCK: TEXT EDITOR -->
	<section class="flex text wysiwyg">
		<g-container>
			<g-row>
				<g-col>
					<?php echo get_sub_field('embed'); ?>
				</g-col>
			</g-row>
		</g-container>
	</section>

<?php endwhile; endif; ?>