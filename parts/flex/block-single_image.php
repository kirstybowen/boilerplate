<?php

	/////////////////////////
	// BLOCK: SINGLE IMAGE //
	/////////////////////////
	$groupName = 'block_group_single_image';

	if ( have_rows( $groupName ) ) : while ( have_rows( $groupName) ) : the_row();

?>

	<!-- BLOCK: SINGLE IMAGE -->
	<section class="flex single-image">
		<g-container>
			<g-row>
				<g-col>
					<?php output_bs_the_acf_thumbnail(get_sub_field('image'), 'large', false); ?> 
				</g-col>
			</g-row>
		</g-container>
	</section>

<?php endwhile; endif; ?>