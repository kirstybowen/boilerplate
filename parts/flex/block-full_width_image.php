<?php

	/////////////////////////////
	// BLOCK: FULL-WIDTH IMAGE //
	/////////////////////////////
	$groupName = 'block_group_full_width_image';

	if ( have_rows( $groupName ) ) : while ( have_rows( $groupName) ) : the_row();

?>

	<!-- BLOCK: FULL WIDTH IMAGE -->
	<section class="flex full-width-image np has-background">
		<figure>
			<?php output_bs_the_acf_thumbnail(get_sub_field('image'), 'large', false); ?> 
		</figure>
	</section>

<?php endwhile; endif; ?>