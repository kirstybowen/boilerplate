<?php

	////////////////////////
	// BLOCK: ACCORDION //
	////////////////////////
$groupName = 'block_group_accordion';

if ( have_rows( $groupName ) ) : while ( have_rows( $groupName) ) : the_row();

	// FIELDS
	$sections =  get_sub_field('section'); 
	$intro = get_sub_field('block_intro');
 


	?>

	<section class="flex accordion">
		<g-container>
			<g-row>
				<g-col>
					<?php if(!empty($intro['intro_title'])){ ?><h3><?php echo $intro['intro_title']; ?></h3><?php } ?> 
					<?php if(!empty($intro['intro_text'])){ ?><p><?php echo $intro['intro_text']; ?></p><?php } ?>
					
					<?php 
					if( have_rows('section') ):

						?>
						<dl>
							<?php

							while( have_rows('section') ) : the_row();

								$section_title = get_sub_field('title');  
								$section_text = get_sub_field('text');  
								?>

								<dt><button data-question><?php echo $section_title; ?></button></dt>
								<dd><answer-content><?php echo $section_text; ?></answer-content></dd>
								<?php
							endwhile;
							?>
						</dl>
						<?php
					endif;

					?>
				</g-col>
			</g-row>
		</g-container>
	</section>

	<?php endwhile; endif; ?>