<?php
  
	$name = get_the_title($args['post_id']); 	
	$type = get_post_type($args['post_id']); 					 
	$link = get_the_permalink($args['post_id']);
	$date = get_the_time('jS F Y',$args['post_id']);

?>

	<a class="post" href="<?php echo $link; ?>"> 
		<figure>
			<?php bs_the_post_thumbnail('medium', true, $args['post_id']); ?> 
		</figure>
		<small><?php echo $type; ?></small>
		<h2><?php echo $name; ?></h2>
	</a>
 

 