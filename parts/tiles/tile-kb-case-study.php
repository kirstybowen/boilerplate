<?php
	
	// POST DATA
	$name = get_the_title($args['post_id']); 					 
	$link = get_the_permalink($args['post_id']); 

?>

	<a class="post" href="<?php echo $link; ?>"> 
		<figure>
			<?php bs_the_post_thumbnail( 'medium', true,$args['post_id']); ?> 
		</figure>
		<h2><?php echo $name; ?></h2> 
	</a>
